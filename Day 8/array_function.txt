/******************* Array Function ***************************/

1.array_chunk()             11.array_push()         21.array_values()   
2.array_column()            12.array_pop()          22.arsort()
3.array_diff()              13.array_rand()         23.compact()
4.array_diff_assoc()        14.array_replace()      24.count()
5.array_fill()              15.array_reverse()      25.in_array()
6.array_intersect()         16.array_search()       26.in_array()
7.array_key_exists()        17.array_shift()        27.ksort()
8.array_keys()              18.array_unshift()      28.krsort()
9.array_merge()             19.array_sum()          29.shuffle()
10.array_pad()              20.array_unique()
