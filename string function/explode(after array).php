<?php
/**
 * Created by PhpStorm.
 * User: Rahmat
 * Date: 3/17/2017
 * Time: 8:20 PM
 */
$pizza  = "piece1 piece2 piece3 piece4 piece5 piece6";
$pieces = explode(" ", $pizza);
echo $pieces[0]."<br>";
echo $pieces[1]."<br>";
echo $pieces[2]."<br>";
echo $pieces[3]."<br>";
echo $pieces[4]."<br>";
echo $pieces[5];


/******** Example 2 **********/

$input1 = "hello";
$input2 = "hello,there";
$input3 = ",";

var_dump(explode(',',$input1));
var_dump(explode(',',$input2));
var_dump(explode(',',$input3));


/******** Example 3 **********/



$str = 'one|two|three|four';

// positive limit
var_dump(explode('|', $str, 2))."<br>";

// negative limit (since PHP 5.1)
var_dump(explode('|', $str, -1));

