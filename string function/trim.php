<?php
/**
 * Created by PhpStorm.
 * User: Rahmat
 * Date: 3/17/2017
 * Time: 9:46 PM
 */
$text   = "\t\tThese are a few words :)... ";
$binary = "\x09Example string\x0A";
$hello  = "Hello World";

var_dump($text, $binary, $hello);

print "\n";

$trimed = trim($text);
var_dump($trimed);

$trimed = trim($text, " \t.");
var_dump($trimed);

$trimed = trim($hello, "Hdle");
var_dump($trimed);

$trimed = trim($hello, "HdWr");
var_dump($trimed);

// trim the ASCII control characters at the beginning and end of $binary
// (from 0 to 31 inclusive)

$clean = trim($binary,"\x00..\x1F");
var_dump($clean);





/************** Example 2 **************************/


function trim_value(&$value)
{
    $value = trim($value);
}


$fruit = array('apple', '          banana', ' cherry');
var_dump($fruit);

array_walk($fruit, 'trim_value');
var_dump($fruit);
